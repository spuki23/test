package a22Basket;

public class Basket {

//    Stwórz klase Basket, ktora imituje koszyk i przechowuje aktualna ilosc elementow w koszyku.
//    Dodaj metode addToBasket(), ktora dodaje element do koszyka (zwiekszajac aktualny stan o 1 )
//    oraz metode removeFromBasket(), ktora usuwa element z koszyka (zmniejszajac aktualny stan o 1
//    Koszyk może przechowywac od 0 do 10 elementow.
//    W przypadku, kiedy uxytkownik chce wykonac akcje usuniecia przy stanie 0 lub dodania przy stanie 10,
//    rzuc odpowiedni runtime exception BasketFullException lub BasketEmptyException.

    //TODO
//    Zamień wyjątki BasketFullException oraz BasketEmptyException z poprzedniego zadania na wyjątki
//    typu checked exception. Obsłuż je.

    int counter = 0;

    public int addToBasket(){
    if (counter == 10) {
        throw new BasketFullException();
    }
            return counter++;
        }

    public int removeFromBasket(){
    if (counter ==0) {
        throw new BasketEmptyException("pusty koszyk");
    }
        return counter--;
    }


    @Override
    public String toString() {
        return "Basket{" +
                "counter=" + counter +
                '}';
    }
}
