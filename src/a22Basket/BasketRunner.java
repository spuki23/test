package a22Basket;

public class BasketRunner {
    public static void main(String[] args) {
        Basket basket = new Basket();
        basket.addToBasket();
        basket.addToBasket();
        basket.removeFromBasket();
        basket.removeFromBasket();
        try{
            basket.removeFromBasket();
        } catch (BasketEmptyException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(basket);





    }
}
