package a22Basket;

public class BasketEmptyException extends RuntimeException{
    public BasketEmptyException(String message) {
        super(message);
    }
}
